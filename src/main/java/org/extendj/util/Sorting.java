/* Copyright (c) 2018, Jesper Öqvist <jesper.oqvist@cs.lth.se>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.extendj.util;

import org.extendj.ast.TypeDecl;
import se.llbit.json.JsonObject;
import se.llbit.json.JsonValue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Helper functions for sorting collections.
 */
public class Sorting {
  public interface KeyFun<T, R> {
    R apply(T t);
  }

  // These can be converted to Java 8 lambdas:

  public static final KeyFun<JsonValue, String> objectNameFun = new KeyFun<JsonValue, String>() {
    @Override public String apply(JsonValue value) {
      return value.object().get("name").asString("");
    }
  };

  public static final KeyFun<JsonObject, String> objectNameKeyFun = new KeyFun<JsonObject, String>() {
    @Override public String apply(JsonObject value) {
      return value.object().get("name").asString("");
    }
  };

  public static final KeyFun<TypeDecl, String> typeNameFun = new KeyFun<TypeDecl, String>() {
    @Override public String apply(TypeDecl type) {
      return type.name();
    }
  };
  public static final KeyFun<JsonValue, String> jsonStringFun = new KeyFun<JsonValue, String>() {
    @Override public String apply(JsonValue value) {
      return value.asString("");
    }
  };

  /**
   * Returns a sorted JSON array based on the argument elements and a key function.
   *
   * <p>The result preserves duplicate elements and elements with identical keys.</p>
   */
  public static <T, K extends Comparable<? super K>> List<T> sortBy(
      Iterable<T> iterable,
      KeyFun<T, K> keyFun) {
    Map<K, Collection<T>> members = new TreeMap<>();
    for (T value : iterable) {
      K sortKey = keyFun.apply(value);
      Collection<T> items = members.get(sortKey);
      if (items == null) {
        items = new ArrayList<>();
        members.put(sortKey, items);
      }
      items.add(value);
    }
    List<K> keys = new ArrayList<>(members.keySet());
    Collections.sort(keys);
    ArrayList<T> result = new ArrayList<>(members.size());
    for (Collection<T> values : members.values()) {
      result.addAll(values);
    }
    return result;
  }

}
