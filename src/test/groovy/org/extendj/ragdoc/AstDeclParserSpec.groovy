/* Copyright (c) 2018, Jesper Öqvist <jesper.oqvist@cs.lth.se>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.extendj.ragdoc

import spock.lang.*

class AstDeclParserSpec extends Specification {

  def "No children (no extends)"() {
    when:
    def res
    new AstDeclParser("Decl;").withCloseable { parser ->
      res = parser.parse()
    }

    then:
    res.toCompactString() == '{"n":"Decl"}'
  }

  def "No children (extends)"() {
    when:
    def res
    new AstDeclParser("IdDecl:Decl;").withCloseable { parser ->
      res = parser.parse()
    }

    then:
    res.toCompactString() == '{"n":"IdDecl","e":"Decl"}'
  }

  def "No children (extends) + whitespace"() {
    when:
    def res
    new AstDeclParser("IdDecl  :  Decl   ;").withCloseable { parser ->
      res = parser.parse()
    }

    then:
    res.toCompactString() == '{"n":"IdDecl","e":"Decl"}'
  }

  def "Binary declaration"() {
    when:
    def res
    new AstDeclParser("Binary ::= Left:Expr Right:Expr;").withCloseable { parser ->
      res = parser.parse()
    }

    then:
    res.toCompactString() == '{"n":"Binary","c":[{"n":"Left","e":"Expr"},{"n":"Right","e":"Expr"}]}'
  }

  def "List child"() {
    when:
    def res
    new AstDeclParser("Program ::= CompilationUnit*;").withCloseable { parser ->
      res = parser.parse()
    }

    then:
    res.toCompactString() == '{"n":"Program","c":[{"e":"CompilationUnit","k":"list"}]}'
  }

  def "Named List child"() {
    when:
    def res
    new AstDeclParser("Program ::= Unit:CompilationUnit*;").withCloseable { parser ->
      res = parser.parse()
    }

    then:
    res.toCompactString() == '{"n":"Program","c":[{"n":"Unit","e":"CompilationUnit","k":"list"}]}'
  }

  def "Named List child + whitespace"() {
    when:
    def res
    new AstDeclParser("Program ::= Unit : CompilationUnit *;").withCloseable { parser ->
      res = parser.parse()
    }

    then:
    res.toCompactString() == '{"n":"Program","c":[{"n":"Unit","e":"CompilationUnit","k":"list"}]}'
  }

  def "Opt child"() {
    when:
    def res
    new AstDeclParser("Return ::= [Expr];").withCloseable { parser ->
      res = parser.parse()
    }

    then:
    res.toCompactString() == '{"n":"Return","c":[{"e":"Expr","k":"opt"}]}'
  }

  def "Named Opt child"() {
    when:
    def res
    new AstDeclParser("Return ::= [Result:Expr];").withCloseable { parser ->
      res = parser.parse()
    }

    then:
    res.toCompactString() == '{"n":"Return","c":[{"n":"Result","e":"Expr","k":"opt"}]}'
  }

  def "Named Opt child + whitespace"() {
    when:
    def res
    new AstDeclParser("Return ::= [ Result : Expr ] ;").withCloseable { parser ->
      res = parser.parse()
    }

    then:
    res.toCompactString() == '{"n":"Return","c":[{"n":"Result","e":"Expr","k":"opt"}]}'
  }

  def "Token child"() {
    when:
    def res
    new AstDeclParser("Number ::= <NUM>;").withCloseable { parser ->
      res = parser.parse()
    }

    then:
    res.toCompactString() == '{"n":"Number","c":[{"n":"NUM","e":"String","k":"token"}]}'
  }

  def "Named Token child"() {
    when:
    def res
    new AstDeclParser("Number ::= <NUM:Integer>;").withCloseable { parser ->
      res = parser.parse()
    }

    then:
    res.toCompactString() == '{"n":"Number","c":[{"n":"NUM","e":"Integer","k":"token"}]}'
  }
}
