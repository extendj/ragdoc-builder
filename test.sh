#!/bin/bash

set -eu

OUTDIR="${1:-data}"

echo "OUTDIR: ${OUTDIR}"

gradle jar
java -jar ragdoll2.jar \
	$(find src/gen/ -name '*.java') \
	$(find extendj/src/frontend -name '*.java') \
	-d "${OUTDIR}"
